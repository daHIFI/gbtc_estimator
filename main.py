import ccxt
import pandas_datareader as web
import pandas as pd
from datetime import date, time, datetime, timedelta

from dotenv import load_dotenv
load_dotenv()

DATES_TO_GO_BACK = 3


def get_dates(num_days=DATES_TO_GO_BACK):
    end_date = date.today()
    start_date = end_date - timedelta(days=num_days)
    return end_date, start_date


class Equity:
    end_date, start_date = get_dates()

    def __init__(self, ticker):
        self.ticker = ticker
        self.ohlcv = self.get_ohlcv()

    def __str__(self):
        return self.ticker

    def __repr__(self):
        return f"Equity('{self.ticker}')"

    def get_ohlcv(self):
        df = web.DataReader(self.ticker, 'iex', Equity.start_date, Equity.end_date)
        df_index_to_datetime(df)
        return df

    def last_session(self):
        return self.ohlcv.iloc[-1]

    def last_close(self):
        return self.last_session().close

    def last_session_date(self):
        return self.last_session().name

    def close_time(self):
        market_close = time(hour=16)
        close_time = datetime.combine(self.last_session_date(), market_close)
        return close_time.timestamp()


class Crypto:
    exchange = ccxt.gemini()

    def __init__(self, symbol):
        self.symbol = symbol
        self.trade_pair = symbol + "/USD"
        self.ticker = Crypto.exchange.fetch_ticker(self.trade_pair)
        self.ohlcv = Crypto.exchange.fetch_ohlcv(self.trade_pair, '1h')

    def __str__(self):
        return self.symbol

    def __repr__(self):
        return f"Crypto('{self.symbol}')"

    def ohlcv_at_time(self, timestamp):
        result = [x for x in self.ohlcv if x[0]/1000 == timestamp]
        return result

    def last_close(self):
        return self.ticker['close']


class Estimator:
    def __init__(self, equity, crypto):
        self.equity = Equity(equity)
        self.crypto = Crypto(crypto)

    def __str__(self):
        return f"{self.equity}\\{self.crypto}"

    def report(self):
        return f'{self.equity}: {self.estimate()} Change: {self.difference()} ({self.percent() * 100})'

    def estimate(self):
        per_share = self.per_share()
        crypto_last = self.crypto.ticker['last']
        return crypto_last * per_share

    def difference(self):
        difference = self.estimate() - self.equity.last_close()
        return difference

    def per_share(self):
        close_timestamp = self.equity.close_time()
        crypto_ohlcv = self.crypto.ohlcv_at_time(close_timestamp)
        crypto_close = crypto_ohlcv[0][4]
        equity_close = self.equity.last_close()
        per_share = equity_close / crypto_close
        return per_share

    def percent(self):
        percent = 1 - ((self.estimate() - self.difference()) / self.estimate())
        return percent


def df_index_to_datetime(df):
    df.index = pd.to_datetime(df.index)
    return


if __name__ == '__main__':
    gbtc = Estimator('GBTC', 'BTC')
    print(gbtc.report())




