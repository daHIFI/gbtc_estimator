GBTC Price Estimator
===

GBTC is the stock ticker for Greyscale Investment's Bitcoin Investment Trust, which trades on the equities markets. Each share of GBTC is equivalent to approximately 0.000969 bitcoin, and will decrease over time as a 2% management fee is taken from the fund. Some Bitcoin advocates may scoff at the idea of holding such an asset, but for some it makes sense. Greyscale handles the custody of BTC, making it simple for less sophsiticated investors. GBTC is also one of the only ways to hold BTC in a retirement account such as an IRA. Because of, or in spite of these factors, the price of GBTC trades at premium, usually 10-25% over the underlying value of BTC.

Since GBTC trades on the US OTC markets, trading takes place during the standard market hours, Monday-Friday 9AM-4PM EST. BTC, as we know, trades 24/7, 365 days a year. The purpose behind this simple estimation program is to give us an idea of what the price of GBTC should be given BTC's afterhours action. 

I've constantly found myself calculating what this price would be during big moves in BTC's price overnight or on weekends, and I hope this is useful for others. 

---
Features: 
+ get crypto prices (from Gemini)
+ get daily close price of equity (via IEX)
+ get timestamp of daily equity close
+ get crypto price at time of equity close
+ calculate the per_share of equity to the underlying crypto value (premium not considered)
+ estimate the price of equity given crypto's current price
+ works with both GBTC and ETHE

---
Pipeline: 
- output to html server
- refresh automatically every 15 minutes. 
- better equity data; realtime

